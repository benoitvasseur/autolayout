//
//  main.m
//  Autolayout
//
//  Created by Benoit on 28/07/2015.
//  Copyright (c) 2015 Cardiweb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
